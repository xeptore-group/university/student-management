#include <vector>
#include <algorithm>

#include "student.hpp"
#include "store.hpp"

using namespace std;

Store::Store()
{
    this->students = vector<Student *>();
}

Store::~Store() {}

Student *Store::findStudentById(string id)
{
    for (auto student = this->students.begin(); student != this->students.end(); student++)
    {
        if ((*student)->getId() == id)
        {
            auto index = distance(this->students.begin(), student);
            return this->students.at(index);
        }
    }

    return NULL;
}

bool Store::studentExists(string id)
{
    for (auto student = this->students.begin(); student != this->students.end(); student++)
    {
        if ((*student)->getId() == id)
        {
            return true;
        }
    }

    return false;
}

bool byFirstNameAsc(Student *studentA, Student *studentB)
{
    return studentA->getFirstName() < studentB->getFirstName();
}

bool byFirstNameDsc(Student *studentA, Student *studentB)
{
    return studentA->getFirstName() > studentB->getFirstName();
}

bool byLastNameAsc(Student *studentA, Student *studentB)
{
    return studentA->getLastName() < studentB->getLastName();
}

bool byLastNameDsc(Student *studentA, Student *studentB)
{
    return studentA->getLastName() > studentB->getLastName();
}

bool byTotalAverageAsc(Student *studentA, Student *studentB)
{
    return studentA->getTotalAverage() < studentB->getTotalAverage();
}

bool byTotalAverageDsc(Student *studentA, Student *studentB)
{
    return studentA->getTotalAverage() > studentB->getTotalAverage();
}

bool byEntranceYearAsc(Student *studentA, Student *studentB)
{
    return studentA->getEntranceYear() < studentB->getEntranceYear();
}

bool byEntranceYearDsc(Student *studentA, Student *studentB)
{
    return studentA->getEntranceYear() > studentB->getEntranceYear();
}

vector<Student *> Store::getStudents(StudentsSortBy sortBy, bool ascending)
{
    switch (sortBy)
    {
    case StudentsSortBy::FirstName:
    {
        if (ascending)
        {
            sort(this->students.begin(), this->students.end(), byFirstNameAsc);
        }
        else
        {
            sort(this->students.begin(), this->students.end(), byFirstNameDsc);
        }
        break;
    }
    case StudentsSortBy::LastName:
    {
        if (ascending)
        {
            sort(this->students.begin(), this->students.end(), byLastNameAsc);
        }
        else
        {
            sort(this->students.begin(), this->students.end(), byLastNameDsc);
        }
        break;
    }
    case StudentsSortBy::TotalAverage:
    {
        if (ascending)
        {
            sort(this->students.begin(), this->students.end(), byTotalAverageAsc);
        }
        else
        {
            sort(this->students.begin(), this->students.end(), byTotalAverageDsc);
        }
        break;
    }
    case StudentsSortBy::EntranceYear:
    {
        if (ascending)
        {
            sort(this->students.begin(), this->students.end(), byEntranceYearAsc);
        }
        else
        {
            sort(this->students.begin(), this->students.end(), byEntranceYearDsc);
        }
        break;
    }
    default:
        break;
    }
    return this->students;
}

uint Store::getStudentsCount()
{
    return this->students.size();
}

void Store::addStudent(Student *student)
{
    this->students.push_back(student);
}

void Store::deleteStudentById(string id)
{
    for (auto student = this->students.begin(); student != this->students.end(); student++)
    {
        if ((*student)->getId() == id)
        {
            this->students.erase(student);
            return;
        }
    }
}

auto isEqualStudent = [](string studentId) {
    return [studentId](Student *student) {
        return (student->getId() == studentId);
    };
};

void Store::replaceStudent(string studentId, Student *student)
{
    replace_if(
        this->students.begin(),
        this->students.end(),
        isEqualStudent(studentId),
        student);
}
