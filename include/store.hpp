#pragma once
#include <string>
#include <vector>

#include "student.hpp"

using namespace std;

enum StudentsSortBy
{
  ID,
  FirstName,
  LastName,
  TotalAverage,
  EntranceYear,
};

class Store
{
private:
  vector<Student *> students;

public:
  Store();
  ~Store();

  Student *findStudentById(string id);
  bool studentExists(string id);
  vector<Student *> getStudents(StudentsSortBy sortedBy, bool ascending = true);
  uint getStudentsCount();

  void addStudent(Student *student);
  void deleteStudentById(string id);
  void replaceStudent(string studentId, Student *student);
};
