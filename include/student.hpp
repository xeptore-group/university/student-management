#pragma once
#include <string>
#include <vector>

using namespace std;

class Student
{
private:
  string id;
  string firstName;
  string lastName;
  uint entranceYear;
  float totalAverage;

public:
  Student(string id);
  ~Student();

  string getId();
  string getFirstName();
  string getLastName();
  string getFullName();
  uint getEntranceYear();
  float getTotalAverage();

  Student *setFirstName(string firstName);
  Student *setLastName(string lastName);
  Student *setEntranceYear(uint entranceYear);
  Student *setTotalAverage(float totalAverage);
};
