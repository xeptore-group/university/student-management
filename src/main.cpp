#include <iostream>
#include <limits>
#include <thread>
#include <chrono>

#include "student.hpp"
#include "store.hpp"
#include "util.hpp"

using namespace std;

void banner();
void menuHeader();
void help();
void editMenu();
void listMenu();
void sortOrderMenu();
int getaction(int max);

void addStudent(Store *store);
void deleteStudent(Store *store);
void editStudent(Store *store);
void listStudents(Store *store);

int main()
{
    auto store = new Store();

    banner();

    while (true)
    {
        help();
        int action = 0;
        while ((action = getaction(4)) == -1)
        {
            error("Invalid input.");
            error("Only enter number of the option you want.");
            help();
        }

        println();

        switch (action)
        {
        case 0:
        {
            exit(0);
        }
        case 1:
        {
            addStudent(store);
            break;
        }
        case 2:
        {
            deleteStudent(store);
            break;
        }
        case 3:
        {
            editStudent(store);
            break;
        }
        case 4:
        {
            listStudents(store);
            break;
        }
        default:
            break;
        }

        // sleeps for 3 seconds before reprinting help
        {
            this_thread::sleep_for(chrono::seconds(3));
        }
    }

    return 0;
}

void banner()
{
    println();
    println(yellow("****************************************************"));
    println(yellow("*                                                  *"));
    println(yellow("*                    Welcome to                    *"));
    println(yellow("*              Student Management App              *"));
    println(yellow("*                                                  *"));
    println(yellow("****************************************************"));
}

void menuHeader()
{
    cout << endl
         << endl
         << endl;
    println(red("****************************************************"));
}

void help()
{
    menuHeader();
    println("Choose one the following options:");
    println("[ " + yellow("1") + " ] " + "Add a student");
    println("[ " + yellow("2") + " ] " + "Delete a student");
    println("[ " + yellow("3") + " ] " + "Edit a student details");
    println("[ " + yellow("4") + " ] " + "List all students");
    println("[ " + grey("0") + " ] " + "Exit");
    cout << endl;
}

void editMenu()
{
    println("Which one do you want to edit:");
    println("[ " + yellow("1") + " ] " + "Student first name");
    println("[ " + yellow("2") + " ] " + "Student last name");
    println("[ " + yellow("3") + " ] " + "Student entrance year");
    println("[ " + yellow("4") + " ] " + "Student total average");
    println("[ " + grey("0") + " ] " + "Cancel");
    cout << endl;
}

void listMenu()
{
    println("Sort students list by:");
    println("[ " + yellow("1") + " ] " + "ID [Default]");
    println("[ " + yellow("2") + " ] " + "First name");
    println("[ " + yellow("3") + " ] " + "Last name");
    println("[ " + yellow("4") + " ] " + "Entrance year");
    println("[ " + yellow("5") + " ] " + "Total average");
    println("[ " + grey("0") + " ] " + "Cancel");
    cout << endl;
}

void sortOrderMenu()
{
    println("Order by:");
    println("[ " + yellow("1") + " ] " + "Ascending order [Default]");
    println("[ " + yellow("2") + " ] " + "Descending order");
    println("[ " + grey("0") + " ] " + "Cancel");
    cout << endl;
}

int getaction(int max)
{
    auto action = promptInt("What can i do for you:");
    if (cin.fail() || action > max || action < 0)
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        return -1;
    }
    return action;
}

void addStudent(Store *store)
{
    string firstName, lastName, id;
    uint entranceYear;
    float totalAverage;

    info("Enter new student information:");

    firstName = promptString("First Name:");
    lastName = promptString("Last Name:");
    id = promptString("Student ID:");
    entranceYear = (uint)promptInt("Entrance Year:");
    totalAverage = promptFloat("Total Average:");

    if (store->studentExists(id))
    {
        error("Student with ID (" + yellowBold(id) + ") already registered.");
        return;
    }

    auto student = new Student(id);
    student->setEntranceYear(entranceYear)
        ->setFirstName(firstName)
        ->setLastName(lastName)
        ->setTotalAverage(totalAverage);

    store->addStudent(student);

    println();
    ok("New student with following information was added successfully:");
    printNewStudentInformation(student);
}

void deleteStudent(Store *store)
{
    info("Enter student ID:");

    string id = promptString("ID:");

    if (!store->studentExists(id))
    {
        error("Student with ID (" + yellowBold(id) + ") is not registered.");
        return;
    }

    store->deleteStudentById(id);

    println();
    ok("Student with ID (" + yellowBold(id) + ") successfully deleted.");
}

void editStudent(Store *store)
{
    info("Enter student ID:");

    string id = promptString("ID:");

    if (!store->studentExists(id))
    {
        error("Student with ID (" + yellowBold(id) + ") is not registered.");
        return;
    }

    const auto oldStudent = store->findStudentById(id);

    const auto updatedStudent = new Student(id);
    updatedStudent
        ->setFirstName(oldStudent->getFirstName())
        ->setLastName(oldStudent->getLastName())
        ->setEntranceYear(oldStudent->getEntranceYear())
        ->setTotalAverage(oldStudent->getTotalAverage());

    while (true)
    {
        editMenu();
        int action = 0;
        bool mustExit = false;
        while ((action = getaction(4)) == -1)
        {
            error("Invalid input.");
            error("Only enter number of the option you want.");
            editMenu();
        }

        println();

        switch (action)
        {
        case 0:
        {
            ok("Editing student information canceled.");
            return;
        }
        case 1:
        {
            string firstName = promptString("New first name:");
            updatedStudent->setFirstName(firstName);
            mustExit = true;
            break;
        }
        case 2:
        {
            string lastName = promptString("New last name:");
            updatedStudent->setLastName(lastName);
            mustExit = true;
            break;
        }
        case 3:
        {
            uint entranceYear = (uint)promptInt("New entrance year:");
            updatedStudent->setEntranceYear(entranceYear);
            mustExit = true;
            break;
        }
        case 4:
        {
            auto totalAverage = promptFloat("Total Average:");
            updatedStudent->setTotalAverage(totalAverage);
            mustExit = true;
            break;
        }
        default:
            break;
        }

        if (mustExit)
        {
            break;
        }
    }

    store->replaceStudent(id, updatedStudent);

    println();
    ok("Student with ID (" + yellowBold(id) + ") successfully updated.");
}

void listStudents(Store *store)
{
    auto studentsCount = store->getStudentsCount();
    if (studentsCount == 0)
    {
        info("No student has been registered yet.");
        println();
        return;
    }

    string plural = "";
    if (studentsCount > 1)
    {
        plural = "s";
    }

    StudentsSortBy sortBy = StudentsSortBy::ID;

    while (true)
    {
        listMenu();
        int action = 0;
        bool mustExit = false;
        while ((action = getaction(5)) == -1)
        {
            error("Invalid input.");
            error("Only enter number of the option you want.");
            listMenu();
        }

        println();

        switch (action)
        {
        case 0:
        {
            ok("Listing students canceled.");
            return;
        }
        case 1:
        {
            mustExit = true;
            break;
        }
        case 2:
        {
            sortBy = StudentsSortBy::FirstName;
            mustExit = true;
            break;
        }
        case 3:
        {
            sortBy = StudentsSortBy::LastName;
            mustExit = true;
            break;
        }
        case 4:
        {
            sortBy = StudentsSortBy::EntranceYear;
            mustExit = true;
            break;
        }
        case 5:
        {
            sortBy = StudentsSortBy::TotalAverage;
            mustExit = true;
            break;
        }
        default:
            break;
        }

        if (mustExit)
        {
            break;
        }
    }

    bool orderByAscending = true;
    while (true)
    {
        sortOrderMenu();
        int action = 0;
        bool mustExit = false;
        while ((action = getaction(2)) == -1)
        {
            error("Invalid input.");
            error("Only enter number of the option you want.");
            sortOrderMenu();
        }

        println();

        switch (action)
        {
        case 0:
        {
            ok("Listing students canceled.");
            return;
        }
        case 1:
        {
            mustExit = true;
            break;
        }
        case 2:
        {
            orderByAscending = false;
            mustExit = true;
            break;
        }
        default:
            break;
        }

        if (mustExit)
        {
            break;
        }
    }

    info("(" + yellowBold(to_string(studentsCount)) + ") student" + plural + " has been registered.");
    info("Listing all in order:");

    const auto students = store->getStudents(sortBy, orderByAscending);

    for (auto student = students.begin(); student != students.end(); student++)
    {
        printRegisteredStudentInformation(*student);
        println();
    }
}
