#include <string>
#include <vector>
#include <iostream>
#include <limits>
#include <thread>
#include <chrono>

#include "student.hpp"

using namespace std;

/*
 * Student class constructor
 *
 * @param id student unique identifier.
*/
Student::Student(string id)
{
    this->id = id;
}

Student::~Student() {}

string Student::getId()
{
    return this->id;
}

string Student::getFirstName()
{
    return this->firstName;
}

string Student::getLastName()
{
    return this->lastName;
}

string Student::getFullName()
{
    return this->firstName + " " + this->lastName;
}

uint Student::getEntranceYear()
{
    return this->entranceYear;
}

float Student::getTotalAverage()
{
    return this->totalAverage;
}

Student *Student::setFirstName(string firstName)
{
    this->firstName = firstName;
    return this;
}

Student *Student::setLastName(string lastName)
{
    this->lastName = lastName;
    return this;
}

Student *Student::setEntranceYear(uint entranceYear)
{
    this->entranceYear = entranceYear;
    return this;
}

Student *Student::setTotalAverage(float totalAverage)
{
    this->totalAverage = totalAverage;
    return this;
}
